import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import Home from './components/page_component/Home';
import HomeWorkEX1 from './components/page_component/HomeWorkEX1';
import ProductDetail from './components/page_component/ProductDetail';
import './components/assets/index.css';


import './custom.css'

export default class App extends Component {
  static displayName = App.name; 

  render () {
      return (
          <Switch>
              <Route exact path='/' component={HomeWorkEX1} />
              <Route exact path='/product-detail' component={ProductDetail} />
          </Switch>
    );
  }
}
