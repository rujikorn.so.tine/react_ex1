﻿import React, { Component } from 'react';
import $ from 'jquery';

class GridItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
        };
    }

    componentDidMount() {

    }


    render() {

        const products = this.props.items;
        return (
            <React.Fragment>
                <div className="item-grid">
                    {
                        products.map((product) =>

                            <div key={product.ProductID} className="item-grid-broder" >
                                <div className="item-grid-img">
                                    <img src={product.ProductImgURL_TH === null ? require('../assets/image/noImage.jpg') : product.ProductImgURL_TH } />
                                </div>
                                <div className="item-grid-details-text" > {product.ProductCatTH} </div>
                                <div className="item-grid-details-text" > {product.ProductNameTH} </div>
                                <div className="item-grid-details-text-bold" > {product.FullPrice} </div>
                            </div>
                        )
                    }
                </div>
            </React.Fragment>
        );
    };
}

export default GridItem;