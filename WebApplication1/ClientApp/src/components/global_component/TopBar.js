﻿import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';


class TopBar extends React.Component {




    render() {
        const pageID = this.props.Page.pageID;
        console.log(this.props.Page);
        return (
            <React.Fragment>
                <div className="navbar">
                    <div className="navbar-title"> H.I.S Service </div>
                    {
                        pageID === 1 ? null : <div className="navbar-back" onClick={() => { this.props.history.goBack() }}  > </div>
                    }
                </div>
 
            </React.Fragment>
        );
    };
}

const mapStateToProps = state => ({
    Page: state.Page,
});



export default withRouter(connect(mapStateToProps, null)(TopBar));