﻿import React, { Component } from 'react';
import $ from 'jquery';

class LayoutBar extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
        };
    }
    
    componentDidMount() {

    }

    listView = () => {
        this.setState({ index: 0 });
        this.props.onListViewClick();
    }

    // Grid View
    gridView = () => {
        this.setState({ index: 1 });
        this.props.onGridViewClick();
    }

    /* Optional: Add active class to the current button (highlight it) */


render() {
    return (
        <React.Fragment>
            <div className="layout-bar">
                <div className="layout-title-bar"> สินค้าทั้งหมด </div>
                <button className={this.state.index === 0 ? "btn active" : "btn"} onClick={ ()=> this.listView() }> List</button>
                <button className={this.state.index === 1 ? "btn active" : "btn"} onClick={() => this.gridView() }> Grid</button>
            </div>
        </React.Fragment>
    );
};
}

export default LayoutBar;