﻿import React, { Component } from 'react';

class SearchBar extends React.Component {

    constructor(props) {
        super(props);
    }

    handleInputChange = (event) => {
        this.props.onInputChange(event);
    };

    render() {
        return (
            <React.Fragment>
                <div className="search-bar">
                    <input type="text" className="search-input-text" placeholder="ค้นหาสินค้า" onChange={this.handleInputChange}></input>
                </div>
            </React.Fragment>
        );
    };
}

export default SearchBar;