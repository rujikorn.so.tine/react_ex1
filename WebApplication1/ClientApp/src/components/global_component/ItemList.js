﻿import React, { Component } from 'react';
import $ from 'jquery';

class ItemList extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            index: 0,

        };

    }

    componentDidMount() {
        console.log(this.props);
    }


    render() {
        console.log(this.props.imagess);

        return (
            <React.Fragment>
                <div className="item-list">
                    <div className="item-list-img">
                        <img src={this.props.imagess === null ? require('../assets/image/noImage.jpg') : this.props.imagess} />
                    </div>
                    <div className="item-list-details">
                        <div className="item-list-details-text"> { this.props.titletxt } </div>
                        <div className="item-list-details-text"> { this.props.detailtxt  } </div>
                        <div className="item-list-details-text-bold"> { this.props.price + ' บาท' } </div>
                    </div>
                </div>
            </React.Fragment>
        );
    };
}

export default ItemList;