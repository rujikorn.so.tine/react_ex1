﻿import React, { Component } from 'react';

class ImageBar extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        const image = this.props.image;
        return (
            <React.Fragment>
                <div className="image-title-bar">

                    <img src={ image } className="image-title-bar-detail" />
                
                </div>
            </React.Fragment>
        );
    };
}

export default ImageBar;