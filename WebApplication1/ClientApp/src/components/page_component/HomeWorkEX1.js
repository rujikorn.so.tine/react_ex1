﻿import React, { Component } from 'react';
import ImageBar from './ImageBar';
import TopBar from '../global_component/TopBarHIS';
import SearchBar from '../global_component/SearchBar';
import LayoutBar from '../global_component/LayoutBar';
import ItemList from '../global_component/ItemList';
import GridItem from '../global_component/GridItem';

import axios from 'axios';
import $ from 'jquery';

class HomeWorkEX1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            JsonResponse: [],
            index: 0,
            round: 0,
            screenWidth: 0,
            imageBar: require('../assets/image/banner2.jpg'),
            mode: 'list',
            searchtxt: '',
            isSearch: false,
        };
        window.addEventListener("resize", this.updateScreenWidth);

    }

    componentDidMount() {
        $(document).ready(() => {
            //$('body').css('background', 'wheat');
        });
        this.LoadData();
    }

    updateScreenWidth = () => {

        this.setState({ screenWidth: window.innerWidth });
    }

    onClickChange() {
        this.setState({ Text: 'Click' });
    }

    LoadData() {
        const endpoint = window.webServiceEndpointHW1;
        const api = '/api/Product/GetAllProduct';
        const apiEndPoint = endpoint + api;


        const body = JSON.stringify({
            PartnerID: 'M004',
            Page_Index: 1,
            Page_Size: window.pageSizeHW1
        });

        axios.post(apiEndPoint, body, { headers: { "Content-Type": "application/json", "Accept": "application/json" } }).then(respones => {
            //console.log('API Respones : ' + JSON.stringify(respones.data.products));
            //const dataAPI = respones.data.products;
            this.setState({ JsonResponse: respones.data.products });
            //console.log('setState: ' + this.state.JsonResponse);

        }).catch((error) => {
            console.log('API Error : ' + error);
        })

    }

    SearchData() {
        const endpoint = window.webServiceEndpointHW1;
        const api = '/api/Product/GetProductByName';
        const apiEndPoint = endpoint + api;        
        const body = JSON.stringify({
            PartnerID: 'M004',
            SearchText: this.state.searchtxt,
            "Lang": "TH"
        });

        axios.post(apiEndPoint, body, { headers: { "Content-Type": "application/json", "Accept": "application/json" } }).then(respones => {
            console.log('API Respones : ' + respones);
            this.setState({ JsonResponse: respones.data.products });
        }).catch((error) => {
            console.log('API Error : ' + error);
        })

    }

    handlelistViewclick = () => {

        this.setState({ imageBar: require('../assets/image/banner2.jpg'), mode: 'list' });
    }

    handleGridViewclick = () => {

        this.setState({ imageBar: require('../assets/image/banner3.jpg'), mode: 'grid' });
    }

    handleInputChange = (event) => {        
        this.setState({ searchtxt: event.currentTarget.value });
        if (event.currentTarget.value !== '') {
            this.SearchData();
            this.setState({ isSearch: true });
        }
        else {
            this.LoadData();
            this.setState({ isSearch: false });
        }
        
    };

    render() {
        const products = this.state.JsonResponse;
        const imageBar = this.state.imageBar;
        const mode = this.state.mode;
        const isSearch = this.state.isSearch;
        return (
            <React.Fragment>
                <TopBar />
                <ImageBar image={imageBar} />
                <SearchBar onInputChange={ this.handleInputChange } />
                <LayoutBar onListViewClick={this.handlelistViewclick} onGridViewClick={this.handleGridViewclick} />
                {
                    products.length === 0 ? isSearch ? <div className="search-no-data"> ไม่มีข้อมูล</div> : <div className="Loader"> <img src={require('../assets/image/loading-buffering.gif')} /></div> :
                        
                            (mode === 'list')
                            ?
                            products.map((product) =>
                                <ItemList key={product.ProductID} imagess={product.ProductImgURL_TH} titletxt={product.ProductCatTH} detailtxt={product.ProductNameTH} price={product.FullPrice} />
                            )

                            :

                            <GridItem items={products} />
                        
                }
                
            </React.Fragment>
        );
    };

}

export default HomeWorkEX1;