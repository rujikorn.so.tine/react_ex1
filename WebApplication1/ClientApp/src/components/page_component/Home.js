﻿import React, { Component } from 'react';
import TopBar from '../global_component/TopBar';
import axios from 'axios';
import $ from 'jquery';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as productAction from '../../actions/product-action';
import * as pageAction from '../../actions/page-action';


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            JsonResponse: [],
            index: 0,
            round: 0,
            screenWidth: 0
        };
        window.addEventListener("resize", this.updateScreenWidth);

    }

    componentDidMount() {
        this.props.PageAction.setPageID(1);
        this.LoadData();
    }


    updateScreenWidth = () => {

        this.setState({ screenWidth: window.innerWidth });
    }

    onClickChange() {
        this.setState({ Text: 'Click' });
    }

    onClick = (products, productId) => {
        this.props.ProductAction.setProductList(products);
        this.props.ProductAction.setProductID(productId);
        this.props.history.push('../product-detail');
    }

    LoadData() {
        const endpoint = window.webServiceEndpoint;
        const api = '/api/Products';
        const apiEndPoint = endpoint + api;

        const body = JSON.stringify({
            SearchText: "",
            ProductCat: 1,
            Status: true,
            PageIndex: this.state.index,
            PageSize: window.pageSize,
            OrderBy: ""
        });

        axios.post(apiEndPoint, body).then(respones => {
            console.log('API Respones : ' + respones);
            if (respones.data !== null) {
                if (this.state.round === 0) {
                    this.setState({ JsonResponse: respones.data.products, index: 1, round: 1 });
                }
                else {
                    let data = this.state.JsonResponse
                    for (var i = 0; i < respones.data.products.length; i++) {
                        data.push({
                            "productId": respones.data.products[i].productId,
                            "productNameEng": respones.data.products[i].productNameEng,
                            "emPrice": respones.data.products[i].emPrice,
                            "imagePath": respones.data.products[i].imagePath,
                        });
                    }
                    this.setState({ JsonResponse: data, index: this.state.index + 1 });
                }
            }
        }).catch((error) => {
            console.log('API Error : ' + error);
        })

    }

    render() {
        const products = this.state.JsonResponse;
        const screenWidth = this.state.screenWidth;
        return (
            <React.Fragment>
                <center>
                    <TopBar />
                    <div className="main-banner">
                        <img src={require('../assets/image/banner.jpg')} style={{ width: '-webkit-fill-available' }} />
                    </div>
                    <div className="sku-home-header">
                        <div className="sku-home-header-title">สินค้าทั้งหมด</div>
                    </div>
                    <div className="productlist-products productlist-products--sku" style={{ gridTemplateColumns: screenWidth <= 500 ? 'repeat(2,1fr)' : 'repeat(3,1fr)'}}>
                        {
                            products.length === 0 ? <h1>Loading...</h1> :
                                <React.Fragment>
                                    {
                                        products.map(({
                                            productId,
                                            productNameEng,
                                            emPrice,
                                            imagePath,
                                        }) =>
                                            <div key={productId} onClick={() => this.onClick(products, productId)}>
                                                <div className="productlist-product">
                                                    <div className="productlist-product-image">
                                                        <img src={imagePath }/>
                                                    </div>
                                                
                                                <div className="productlist-product-data" >
                                                    <div className="productlist-product-data-title" >
                                                        {productNameEng}
                                                    </div>
                                                    <div className="productlist-product-data-price"> { emPrice } บาท </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }
                                </React.Fragment>

                        }
                    </div>
                    <button onClick={()=> this.LoadData() }> Load More</button>
                </center>
            </React.Fragment>
        );
    };

}

const mapDispatchToProps = dispatch => ({
    ProductAction: bindActionCreators(productAction, dispatch),
    PageAction: bindActionCreators(pageAction, dispatch)
});

export default connect(null, mapDispatchToProps)(Home);